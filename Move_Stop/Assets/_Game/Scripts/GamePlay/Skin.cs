using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin : MonoBehaviour
{
    public SkinData[] skinData;
    public int currentIdSkin;

    public Transform hair_hatTF;
    public GameObject pants;
    public Transform leftHandObjectTF;
    public Transform backTF;
    public Transform tailTF;

    GameObject hair_hat;
    GameObject handObject;
    GameObject wingObject;
    GameObject tailObject;

    public void ChangeSkin(int idSkin)
    {
        ResetAllObjectSkin();

        currentIdSkin = idSkin;
        pants.GetComponent<SkinnedMeshRenderer>().materials[0].mainTexture = skinData[idSkin].pants;
        hair_hat = Instantiate(skinData[idSkin].hair_hat, hair_hatTF);
        handObject = Instantiate(skinData[idSkin].handObject, leftHandObjectTF);
        wingObject = Instantiate(skinData[idSkin].wingObject, backTF);
        tailObject = Instantiate(skinData[idSkin].tailObject, tailTF);
    }

    void ResetAllObjectSkin()
    {
        if (hair_hat != null)
        {
            Destroy(hair_hat);
        }
        if (handObject != null)
        {
            Destroy(handObject);
        }
        if (wingObject != null)
        {
            Destroy(wingObject);
        }
        if (tailObject != null)
        {
            Destroy(tailObject);
        }
    }
}
