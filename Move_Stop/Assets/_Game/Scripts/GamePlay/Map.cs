using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public Transform startPoint;

    public SpawnBotInMap zone;

    private void OnEnable()
    {
        OnInit();
    }

    void OnInit()
    {
        OnSpawnBot();
    }

    void OnSpawnBot()
    {
        zone.enabled = true;
    }
}
