using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Skin", menuName = "SkinElements")]
public class SkinData : ScriptableObject
{
    public int idSkin;
    public GameObject hair_hat;
    public Texture pants;
    public GameObject handObject;
    public GameObject wingObject;
    public GameObject tailObject;
}
