using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private GameObject WeaponPosition;

    public Character character;
    internal Transform tf;
    private Weapon weapon;
    // Start is called before the first frame update
    void Start()
    {
        OnInit();
        
    }

    private void OnInit()
    {
        tf = transform;
        WeaponPosition.SetActive(true);
    }

    public void Attack(Vector3 des)
    {
        // tat weapon tren tay
        WeaponPosition.SetActive(false);

        // pooling weapon
        weapon = SimplePool.Spawn<Weapon>(character.poolType, tf.position, tf.rotation);
        //Debug.Log(weapon +"__"+ transform.root.name);
        weapon.GetOwner(character);
        weapon.SetupTarget(this, des);
        weapon.OnInit();
    }

    public void DeAttack()
    {
        // bat lai weapon
        if (character != null)
        {
            WeaponPosition.SetActive(true);
        }
        // cong diem cho char neu hit
        if(weapon.hit != null)
        {
            character.ScoreUP(weapon.hit.level);
            weapon.hit.OnHit();
        }
    }
}
