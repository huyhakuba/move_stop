using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnBotInMap : MonoBehaviour
{
    [SerializeField] float minSpawnDistance;
    [SerializeField] PoolType poolType;
    [SerializeField] GameObject botInMap;
    [SerializeField] int maxBotNumber;
    [SerializeField] float timeDelaySpawn;

    public Vector3 currentBotSpawnPos;
    public List<Bot> currentBots;
    public int currentBotNumber;

    public float range;

    private bool canSpawnAgain;

    private void OnEnable()
    {
        currentBotNumber = currentBots.Count;
        canSpawnAgain = true;
    }

    private void Update()
    {
        if (CheckCanSpawnBot() && canSpawnAgain)
        {
            SpawnBot();
        }
    }

    void SpawnBot()
    {
        canSpawnAgain = false;
        GameManager.Ins.DelayByTime(timeDelaySpawn, delegate
        {
            canSpawnAgain = true;
        }, this.gameObject);

        GameObject newBot = Instantiate(botInMap, currentBotSpawnPos + new Vector3(0, 2, 0), Quaternion.identity);
        newBot.transform.SetParent(transform);
        currentBots.Add(newBot.GetComponent<Bot>());
        currentBotNumber = currentBots.Count;
        UIManager.Ins.GetUI<GamePlay>().AddTargetIndicatorInList();
    }

    public void RemoveBotsInList(Character character)
    {
        //Debug.Log(character + " " + character.transform.position);
        for (int i = 0; i < currentBots.Count; i++)
        {
            if (character == currentBots[i])
            {
                currentBots.RemoveAt(i);
                currentBotNumber = currentBots.Count;
                UIManager.Ins.GetUI<GamePlay>().RemoveTargetIndicatorInList(i);
            }
        }
    }

    bool CheckCanSpawnBot()
    {
        if (RandomPoint(transform.position, range, out currentBotSpawnPos) /*&& GetMinDistance(currentBotSpawnPos, currentBots) >= minSpawnDistance*/ && currentBotNumber < maxBotNumber)
        {
            if (GetMinDistance(currentBotSpawnPos, currentBots) >= minSpawnDistance)
            {
                return true;
            }
            return false;
        }
        return false;
    }

    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
        {
            result = hit.position;
            return true;
        }
        result = Vector3.zero;
        return false;
    }

    float GetMinDistance(Vector3 botPos, List<Bot> bots)
    {
        float distance = 0;
        if (bots != null && bots.Count >= 2)
        {
            distance = Vector3.Distance(botPos, bots[0].gameObject.transform.position);
            for (int i = 1; i < bots.Count; i++)
            {
                if (distance > Vector3.Distance(botPos, bots[i].gameObject.transform.position))
                {
                    distance = Vector3.Distance(botPos, bots[i].gameObject.transform.position);
                }
            }
        }
        else if(bots != null && bots.Count == 1)
        {
            distance = Vector3.Distance(botPos, LevelManager.Ins.currentPlayerInMap.tf.position);
            if (distance > Vector3.Distance(botPos, bots[0].tf.position))
            {
                distance = Vector3.Distance(botPos, bots[0].tf.position);
            }
        }
        else
        {
            distance = Vector3.Distance(botPos, LevelManager.Ins.currentPlayerInMap.tf.position);
        }
        return distance;
    }
}
