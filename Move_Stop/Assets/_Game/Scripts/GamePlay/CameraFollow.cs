using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform cameraTF;
    [SerializeField] Transform targetTF;
    [SerializeField] Vector3 offset;
    [SerializeField] float speed;

    private void Start()
    {
        OnInit();
    }

    private void LateUpdate()
    {
        LevelManager.Ins.CameraFollowToPlayer(cameraTF, offset, speed);
    }

    void OnInit()
    {
        targetTF = LevelManager.Ins.TargetToPlayerInGame();
        //cameraTF.position = targetTF.position + offset;
    }
}
