using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Bot : Character
{
    internal float speed;
    internal Vector3 des;
    // Moving range
    public float movingRange = 100f;

    public IState currentState;
    public NavMeshAgent navMeshAgent;

    [SerializeField] Transform targetCircle;

    private void Update()
    {
        if(currentState != null)
        {
            currentState.OnExecute(this);
        }

        //Debug.Log(CheckTarget()!=null?CheckTarget().name:"NOTHING");
    }

    public override void OnInit()
    {
        base.OnInit();
        RandomScoreAndLevel();
        DataManager.Ins.ChangeWeapon(this, (WeaponType)Random.Range(0, 2));
        ChangeState(new Patrol());
    }

    void RandomScoreAndLevel()
    {
        score = 1;
        level = 1;
    }

    public override void Moving()
    {
        base.Moving();
        if(Vector3.Distance(tf.position, des) > 0.1f)
        {
            navMeshAgent.SetDestination(des);
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }
    }

    public override void OnHit()
    {
        ChangeState(null);
        base.OnHit();
    }

    public override void OnDespawn()
    {
        Debug.Log(this);
        LevelManager.Ins.currentMap.zone.RemoveBotsInList(this);
        Destroy(gameObject);
    }

    public void ChangeState(IState newState)
    {
        if(currentState != null)
        {
            currentState.OnExit(this);
        }
        currentState = newState;
        if(currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public override void Attack()
    {
        if (!isAttack)
        {
            base.Attack();
            isAttack = true;
        }

        
    }

    public void ShowTargetCircle(bool show)
    {
        targetCircle.gameObject.SetActive(show);
    }
}
