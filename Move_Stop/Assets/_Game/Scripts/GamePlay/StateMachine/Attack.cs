using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : IState
{
    private float timeDelayForAttack;
    private float timeAfterAttack;
    private float time;
    public void OnEnter(Bot bot)
    {
        bot.currentTarget = bot.CheckTarget();
        timeAfterAttack = 2f;
        timeDelayForAttack = .7f;
        time = 0f;
    }
    public void OnExecute(Bot bot)
    {
        if(time > timeDelayForAttack)
        {
            bot.Attack();
        }
        if(time > timeAfterAttack)
        {
            bot.ChangeState(new Patrol());
        }
        time += Time.deltaTime;
    }
    public void OnExit(Bot bot)
    {
        bot.isAttack = false;
    }
}