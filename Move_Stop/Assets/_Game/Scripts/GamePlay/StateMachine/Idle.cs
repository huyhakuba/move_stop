using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : IState
{
    private float time;
    private float timer;
    
    public void OnEnter(Bot bot)
    {
        bot.ChangeAnim(Constant.ANIM_IDLE);
        timer = Random.Range(2f, 3f);
        time = 0f;
    }
    public void OnExecute(Bot bot)
    {
        if(bot.CheckTarget() != null)
        {
            bot.ChangeState(new Attack());
        }
        if(time > timer)
        {
            bot.ChangeState(new Patrol());
        }
        time += Time.deltaTime;
    }
    public void OnExit(Bot bot)
    {

    }
}
