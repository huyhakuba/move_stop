using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : GameUnit
{
    internal Vector3 des;
    internal WeaponController weaponController;

    public float speed = 5f;
    internal Character hit;

    public Character owner;

    internal virtual void Update()
    {
        /*
        RaycastHit hit;
        if (Physics.Raycast(TF.position, TF.forward,out hit, 0.3f))
        {
            Character character = hit.collider.gameObject.GetComponent<Character>();
            if(character != null && character != weaponController.character)
            {
                this.hit = character;
                OnDespawn();
            }
        }
        */
        Moving();
    }

    
    public override void OnInit()
    {
        hit = null;
    }

    public virtual void  SetupTarget(WeaponController weaponController, Vector3 des)
    {
        this.weaponController = weaponController;
        this.des = des;
    }
    
    public virtual void Moving()
    {
        TF.position = Vector3.MoveTowards(TF.position, des, speed * Time.deltaTime);
    }


    public override void OnDespawn()
    {
        weaponController.DeAttack();
        SimplePool.Despawn(this);
    }

    public void GetOwner(Character character)
    {
        owner = character;
    }    

    private void OnTriggerEnter(Collider other)
    {
        IHit hit = Cache.GetHit(other);
        Character target = CacheCharacter.GetCharacterTarget(other);
        if(hit == null /*|| hit is Player*/ || target == owner)
        {
            return;
        }
        else if (target != owner)
        {
            owner.ScoreUP(target.score);
            target.OnHit();
        }
        //this.hit = character;
        //Debug.Log(other.name);
        OnDespawn();
    }
}
