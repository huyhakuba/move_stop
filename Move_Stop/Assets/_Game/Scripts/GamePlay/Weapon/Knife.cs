using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : Weapon
{
    public override void Moving()
    {
        base.Moving();
        if(Vector3.Distance(TF.position, des) < 0.1f)
        {
            OnDespawn();
        }
    }
}
