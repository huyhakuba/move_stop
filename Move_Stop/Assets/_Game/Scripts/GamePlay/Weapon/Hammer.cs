using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : Weapon
{
    [SerializeField] Transform childTF;

    bool canComeBack;

    public override void OnInit()
    {
        base.OnInit();
        canComeBack = false;
    }

    public override void Moving()
    {
        childTF.Rotate(new Vector3(0, 5f, 0));
        if (!canComeBack)
        {
            MoveForward();
        }
        else
        {
            MoveBackToOwner();
        }
    }

    void MoveForward()
    {
        if (owner == null || Vector3.Distance(TF.position, des) < 0.5f)
        {
            canComeBack = true;
        }
        else
        {
            TF.position = Vector3.MoveTowards(TF.position, des, speed * Time.deltaTime);
        }
    }

    void MoveBackToOwner()
    {
        if (owner == null || Vector3.Distance(TF.position, owner.transform.position) < 1f)
        {
            canComeBack = false;
            OnDespawn();
        }
        else
        {
            TF.position = Vector3.MoveTowards(TF.position, owner.transform.position, speed * Time.deltaTime);
        }
    }
}
