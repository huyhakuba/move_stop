using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character, IHit
{ 
    [SerializeField] private VariableJoystick joystick;
    [SerializeField] private float speed;

    private void Update()
    {
        // detect bot gan nhat trong vung tan cong cua player
        if(currentTarget != CheckTarget())
        {
            if(currentTarget != null)
            {
                currentTarget.gameObject.GetComponent<Bot>().ShowTargetCircle(false);
            }
            currentTarget = CheckTarget();
            if (currentTarget != null)
            {
                currentTarget.gameObject.GetComponent<Bot>().ShowTargetCircle(true);
            }
            
        }
        
        Moving();
        if(!isMoving && currentTarget != null)
        {
            Attack();
        }
    }

    public override void OnInit()
    {
        base.OnInit();
        joystick = UIManager.Ins.variableJoystick;
        poolType = PoolType.Weapon_2;
        score = 0;
        level = 1;
    }

    public override void Moving()
    {
        base.Moving();
        float xMove = joystick.Horizontal;
        float zMove = joystick.Vertical;

        if (!isDead)
        {
            // add velocity cho player
            rb.velocity = speed * Time.deltaTime * new Vector3(xMove, 0, zMove);
        }

        // quay player theo huong di chuyen 
        if (Vector2.Distance(new Vector2(xMove, zMove), Vector2.zero) > 0.1f && !isDead)
        {
            isMoving = true;
            ChangeAnim(Constant.ANIM_RUN);
            isAttack = false;
            tf.rotation = Quaternion.LookRotation(new Vector3(xMove, 0, zMove), Vector3.up);
        }
        else if (Vector2.Distance(new Vector2(xMove, zMove), Vector2.zero) <= 0.1f && !isDead)
        {
            isMoving = false;
            if(!isAttack)
            {
                ChangeAnim(Constant.ANIM_IDLE);
            }
            
        }
    }

    public override void OnDespawn()
    {
        base.OnDespawn();
    }

    public override void Attack()
    {
        if (!isAttack && currentTarget != null)
        {
            isAttack = true;
            base.Attack();
        }
    }

    public override void OnHit()
    {
        base.OnHit();
        GameManager.Ins.DelayByTime(2f, delegate
        {
            UIManager.Ins.GetUI<GamePlay>().LoseButton();
        }, this.gameObject);
    }
}
