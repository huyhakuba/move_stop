using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetIndicator : UICanvas
{
    public Camera mainCamera;
    public GameObject indicator;
    public List<GameObject> targetIndicatiors;
    public RectTransform indicatorPanel;

    public override void Init()
    {
        base.Init();
    }

    private void Start()
    {
        GetListTargetIndicator();
    }

    void Update()
    {
        for (int i = 0; i < targetIndicatiors.Count; i++)
        {
            Bot fakeBot = LevelManager.Ins.currentMap.zone.currentBots[i];
            if (!CheckObjectInScreen(fakeBot.transform.position))
            {
                targetIndicatiors[i].SetActive(true);
                targetIndicatiors[i].transform.position = GetTargetIndicatior(fakeBot.tf.position);
            }
            else
            {
                targetIndicatiors[i].SetActive(false);
            }
        }
    }

    void GetListTargetIndicator()
    {
        if (LevelManager.Ins.currentMap.zone.currentBots != null && LevelManager.Ins.currentMap.zone.currentBots.Count == 0)
        {
            for (int i = 0; i < LevelManager.Ins.currentMap.zone.currentBots.Count; i++)
            {
                GameObject fakeIndicator = Instantiate(indicator, indicatorPanel);
                targetIndicatiors.Add(fakeIndicator);
            }
        }
    }

    public void AddTargetIndicatorInList()
    {
        GameObject fakeIndicator = Instantiate(indicator, indicatorPanel);
        targetIndicatiors.Add(fakeIndicator);
    }    

    public void RemoveTargetIndicatorInList(int id)
    {
        targetIndicatiors.RemoveAt(id);
    }    

    bool CheckObjectInScreen(Vector3 pointObject)
    {
        return mainCamera.WorldToViewportPoint(pointObject).x <= 1 && mainCamera.WorldToViewportPoint(pointObject).x >= 0
            && mainCamera.WorldToViewportPoint(pointObject).y <= 1 && mainCamera.WorldToViewportPoint(pointObject).y >= 0;
    }

    public Vector2 GetTargetIndicatior(Vector3 pointObject)
    {
        Vector2 viewPoint = new Vector2(Mathf.Clamp(mainCamera.WorldToViewportPoint(pointObject).x, 0.1f, 0.9f),
                                         Mathf.Clamp(mainCamera.WorldToViewportPoint(pointObject).y, 0.05f, 0.95f));
        return mainCamera.ViewportToScreenPoint(viewPoint);
    }
}
