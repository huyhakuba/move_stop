using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IHit
{ 
    public PoolType poolType;
    public float range;

    [SerializeField] private WeaponController weaponController;
    [SerializeField] private LayerMask characterLayer;
    [SerializeField] private Animator anim;

    public Skin skeletonSkin;
    public GameObject WeaponPosition;
    internal GameObject currentWeapon;

    public Transform tf;
    public Collider thisCollider;
    public Rigidbody rb;
    public Transform currentTarget;
    public WeaponType weaponType;


    public int score = 0;
    public int level = 1;
    

    internal bool isMoving;
    internal bool isAttack;
    internal bool isDead;

    internal int[] scoreRange =
    {
        3,7,17,23,45
    };
    [SerializeField] float multiScale;

    private string currentAnimName;

    // Start is called before the first frame update
    private void Start()
    {
        OnInit();
    }

    public virtual void OnInit()
    {
        isAttack = false;
        isMoving = false;
        currentTarget = null;
        isDead = false;

        weaponController.character = this;
    }

    public virtual void OnHit()
    {
        Debug.LogWarning("hit");
        ChangeAnim(Constant.ANIM_DEAD);

        isDead = true;
        thisCollider.enabled = false;

        Invoke(nameof(OnDespawn), 2f);
    }

    public virtual void Moving()
    {

    }
    public virtual void Attack()
    {
            tf.rotation = Quaternion.LookRotation(Vector3.Normalize(new Vector3(currentTarget.position.x - tf.position.x, 0, currentTarget.position.z - tf.position.z)), Vector3.up);
            ChangeAnim(Constant.ANIM_ATTACK);
            weaponController.Attack(currentTarget.position);
    }

    public virtual void OnDespawn()
    {
       
    }

    public void ScoreUP(int score)
    {
        this.score += score;
        LevelUp();
    }

    public void LevelUp()
    {
        level++;
        // scale up character
        tf.localScale *= multiScale;
        tf.position = new Vector3(tf.position.x, tf.localScale.y - 1, tf.position.z);
        range *= multiScale;
    }

    public Transform CheckTarget()
    {
        Transform target = null;
        Collider[] colliders = Physics.OverlapSphere(tf.position, range, characterLayer);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].transform == tf || CacheCharacter.GetCharacterTarget(colliders[i]).isDead)
            {
                continue;
            }
            if (target == null)
            {
                target = colliders[i].transform;
            }
            else
            {
                if (Vector3.Distance(target.position, tf.position) > Vector3.Distance(colliders[i].transform.position, tf.position))
                {
                    target = colliders[i].transform;
                }
            }
        }
        return target;
    }

    public void ChangeAnim(string animName)
    {
        if (currentAnimName != animName)
        {
            anim.ResetTrigger(animName);
            currentAnimName = animName;
            anim.SetTrigger(currentAnimName);
        }
    }
}
