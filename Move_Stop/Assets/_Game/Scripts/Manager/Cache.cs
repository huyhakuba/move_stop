using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Cache 
{
    static Dictionary<Collider, IHit> Collider_IHit_Dic = new Dictionary<Collider, IHit>();

    public static IHit GetHit(Collider collider)
    {
        if(!Collider_IHit_Dic.ContainsKey(collider))
        {
            Collider_IHit_Dic.Add(collider, collider.GetComponent<IHit>());
        }
        return Collider_IHit_Dic[collider];
    }
}

public static class CacheCharacter
{
    static Dictionary<Collider, Character> colliderCharcterDiction = new Dictionary<Collider, Character>();

    public static Character GetCharacterTarget(Collider collider)
    {
        if (!colliderCharcterDiction.ContainsKey(collider))
        {
            colliderCharcterDiction.Add(collider, collider.GetComponent<Character>());
        }
        return colliderCharcterDiction[collider];
    }
}
