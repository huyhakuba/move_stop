using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// check trang thai cua game
public enum GameState 
{ 
    MainMenu, Gameplay, Finish
}

public class GameManager : Singleton<GameManager>
{
    #region Game State
    private GameState state;

    //[SerializeField] UserData userData;
    //[SerializeField] CSVData csv;
    //private static GameState gameState = GameState.MainMenu;

    void Awake()
    {
        //setup tong quan game
        //setup data

        //base.Awake();
        Input.multiTouchEnabled = false;
        Application.targetFrameRate = 60;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        int maxScreenHeight = 1280;
        float ratio = (float)Screen.currentResolution.width / (float)Screen.currentResolution.height;
        if (Screen.currentResolution.height > maxScreenHeight)
        {
            Screen.SetResolution(Mathf.RoundToInt(ratio * (float)maxScreenHeight), maxScreenHeight, true);
        }

        //csv.OnInit();
        //userData?.OnInitData();

        ChangeState(GameState.MainMenu);

        UIManager.Ins.OpenUI<MianMenu>();
    }

    // thay doi trang thai game
    public void ChangeState(GameState gameState)
    {
        state = gameState;
    }

    // check state co dang hoat dong khong 
    public bool IsState(GameState gameState)
    {
        return state == gameState;
    }
    #endregion

    private int _levelUnlock = -1;
    public int levelUnlock
    {
        get
        {
            if (_levelUnlock <= 0)
            {
                _levelUnlock = PlayerPrefs.GetInt("LEVEL_UNLOCK", 1);
            }
            return _levelUnlock;
        }
        set
        {
            _levelUnlock = value;
            PlayerPrefs.SetInt("LEVEL_UNLOCK", value);
        }
    }

    private int _levelPlay = -1;
    public int levelPlay
    {
        get
        {
            if (_levelPlay <= 0)
            {
                _levelPlay = levelUnlock;
            }
            return _levelPlay;
        }
        set
        {
            _levelPlay = value;
        }
    }

    public void PlayGame()
    {
        LevelManager.Ins.LoadMap(1);
    }

    public void DelayByTime(float time, UnityAction callback, GameObject target)
    {
        StartCoroutine(Delay());
        IEnumerator Delay()
        {
            yield return new WaitForSeconds(time);
            if (callback != null && target != null)
            {
                callback.Invoke();
            }
        }
    }

}
