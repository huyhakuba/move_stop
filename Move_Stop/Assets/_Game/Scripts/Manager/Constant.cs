using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    public const string ANIM_IDLE = "idle";
    public const string ANIM_RUN = "run";
    public const string ANIM_ATTACK = "attack";
    public const string ANIM_WIN = "win";
    public const string ANIM_DEAD = "dead";
    public const string ANIM_ULTI = "ulti";
    public const string CHARACTERTAG = "Character";

 
}
