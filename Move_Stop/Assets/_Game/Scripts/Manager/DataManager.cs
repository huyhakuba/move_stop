using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{ 
    Knife,
    Hammer,
    Arrow,
}

public class WeaponSkin
{

}


public class DataManager : Singleton<DataManager>
{
    public SpawnManagerScriptableObject scriptableObject;
    public void ChangeWeapon(Character character, WeaponType weaponType)
    {
        character.weaponType = weaponType;
        character.poolType = (PoolType)(int)weaponType;
        if(character.currentWeapon!=null)
        {
            Destroy(character.currentWeapon);
        }
        character.currentWeapon = Instantiate(scriptableObject.weapon[(int)weaponType], character.WeaponPosition.transform);
    }

    public int GetIdRandomWeaponNotOwned()
    {
        int idCurrentWeaponEquip = (int)LevelManager.Ins.currentPlayerInMap.weaponType;
        int index = idCurrentWeaponEquip;
        while (index == idCurrentWeaponEquip)
        {
            index = Random.Range(0, 2);
        }
        return index;
    }


}
