using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public Map currentMap;

    ResourceRequest requestMap;
    Map currentPrefabMap;

    public void LoadMap(int idMap)
    {
        StartCoroutine(PreLoadMap(idMap));
    }

    IEnumerator PreLoadMap(int idMap)
    {
        if (requestMap == null)
        {
            PreLoadMapAsync(idMap);
        }

        while (!requestMap.isDone)
        {
            yield return null;
        }

        DestroyCurrentMap();
        currentPrefabMap = (Instantiate(requestMap.asset) as Map);
        currentPrefabMap.transform.SetParent(this.transform);
        currentMap = currentPrefabMap;
        requestMap = null;

        LevelManager.Ins.SpawnPlayer();
    }

    public void PreLoadMapAsync(int idMap)
    {
        requestMap = Resources.LoadAsync(string.Format("Map/Map {0}", idMap), typeof(Map));
    }

    public void DestroyCurrentMap()
    {
        DestroyPlayer();
        if (currentPrefabMap != null)
        {
            Destroy(currentPrefabMap.gameObject);
        }
    }

    public Transform TargetToPlayerInGame()
    {
        if (currentPlayerInMap != null)
        {
            return currentPlayerInMap.transform;
        }
        else return null;
    }

    [SerializeField] Player playerPrefab;
    public Player currentPlayerInMap;

    public void CameraFollowToPlayer(Transform cameraTF, Vector3 offset, float smoothSpeed)
    {
        if (currentPlayerInMap != null)
        {
            Vector3 desiredPos = currentPlayerInMap.tf.position + offset;
            Vector3 smoothedPos = Vector3.Lerp(cameraTF.position, desiredPos, smoothSpeed * Time.deltaTime);
            cameraTF.position = smoothedPos;
        }
    }

    public void SpawnPlayer()
    {
        currentPlayerInMap = Instantiate(playerPrefab, currentMap.startPoint.transform.position, Quaternion.identity);
        currentPlayerInMap.transform.SetParent(currentMap.startPoint);
    }

    public void DestroyPlayer()
    {
        if (currentPlayerInMap != null)
        {
            Destroy(currentPlayerInMap.gameObject);
        }
    }

}
