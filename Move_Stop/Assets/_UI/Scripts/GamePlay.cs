using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : UICanvas
{
    public Camera mainCamera;
    public GameObject indicator;
    public List<GameObject> targetIndicatiors;
    public RectTransform indicatorPanel;

    public override void Init()
    {
        base.Init();
        mainCamera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        if (targetIndicatiors != null && targetIndicatiors.Count > 0)
        {
            for (int i = 0; i < targetIndicatiors.Count; i++)
            {
                Bot fakeBot = LevelManager.Ins.currentMap.zone.currentBots[i];
                if (!CheckObjectInScreen(fakeBot.transform.position))
                {
                    targetIndicatiors[i].SetActive(true);
                    //Debug.Log(targetIndicatiors[i].transform.name + " - " + targetIndicatiors[i].transform.position);

                    targetIndicatiors[i].GetComponent<RectTransform>().anchoredPosition = GetTargetIndicatior(fakeBot.transform.position) - new Vector3(Screen.width, Screen.height) / 2;

                    targetIndicatiors[i].GetComponent<RectTransform>().up 
                        = targetIndicatiors[i].GetComponent<RectTransform>().anchoredPosition 
                        - ((Vector2)GetTargetIndicatior(LevelManager.Ins.currentPlayerInMap.transform.position) 
                        - new Vector2(Screen.width, Screen.height) / 2);
                }
                else
                {
                    targetIndicatiors[i].SetActive(false);
                }
            }
        }
    }

    public void AddTargetIndicatorInList()
    {
        GameObject fakeIndicator = Instantiate(indicator, indicatorPanel);
        targetIndicatiors.Add(fakeIndicator);
    }

    public void RemoveTargetIndicatorInList(int id)
    {
        GameObject fakeIndicator = targetIndicatiors[id];
        targetIndicatiors.RemoveAt(id);
        Destroy(fakeIndicator);
    }

    void ClearListTargetIndicator()
    {
        while (targetIndicatiors != null && targetIndicatiors.Count > 0)
        {
            GameObject fakeTarget = targetIndicatiors[0];
            targetIndicatiors.RemoveAt(0);
            Destroy(fakeTarget);
        }
    }

    bool CheckObjectInScreen(Vector3 pointObject)
    {
        return mainCamera.WorldToViewportPoint(pointObject).x <= 1 && mainCamera.WorldToViewportPoint(pointObject).x >= 0
            && mainCamera.WorldToViewportPoint(pointObject).y <= 1 && mainCamera.WorldToViewportPoint(pointObject).y >= 0;
    }

    public Vector3 GetTargetIndicatior(Vector3 pointObject)
    {
        Vector2 viewPoint = mainCamera.WorldToViewportPoint(pointObject);
        viewPoint.x = Mathf.Clamp(viewPoint.x, 0.1f, 0.9f);
        viewPoint.y = Mathf.Clamp(viewPoint.y, 0.1f, 0.9f);

        return mainCamera.ViewportToScreenPoint(viewPoint);
    }

    public void GetTargetIndicatorRotation(Transform indicator, Transform target)
    {
        indicator.LookAt(target, Vector3.forward);
    }

    public void WinButton()
    {
        UIManager.Ins.OpenUI<Win>().score.text = Random.Range(100, 200).ToString();
        Close();
    }

    public void LoseButton()
    {
        UIManager.Ins.OpenUI<Lose>().score.text = Random.Range(0, 100).ToString(); 
        Close();
        LevelManager.Ins.DestroyCurrentMap();
    }

    public void SettingButton()
    {
        UIManager.Ins.OpenUI<Setting>();
    }

    public void ChangSkinButton()
    {
        int index = GetIdRandomSkin();

        if (index != LevelManager.Ins.currentPlayerInMap.skeletonSkin.currentIdSkin)
        {
            LevelManager.Ins.currentPlayerInMap.skeletonSkin.ChangeSkin(index);
        }
    }

    int GetIdRandomSkin()
    {
        int currentIdSkinEquip = LevelManager.Ins.currentPlayerInMap.skeletonSkin.currentIdSkin;
        int index = currentIdSkinEquip;
        while (index == currentIdSkinEquip)
        {
            index = Random.Range(0, 3);
        }
        return index;
    }

    public void ChangeWeaponButton()
    {
        int index = DataManager.Ins.GetIdRandomWeaponNotOwned();
        DataManager.Ins.ChangeWeapon(LevelManager.Ins.currentPlayerInMap, (WeaponType)index);
    }

    public override void Close()
    {
        ClearListTargetIndicator();
        base.Close();
    }
}
